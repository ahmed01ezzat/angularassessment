import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AlertService, AuthenticationService ,RestService,ModalService } from './services';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import { FormBuilder } from '@angular/forms';
// import { AlertComponent } from './directives/alert.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { AlertComponent,ModalComponent } from './directives/index';






@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    DashboardComponent,
    HeaderComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  
  ],
  providers: [ 
    AlertService,
    AuthenticationService,
    RestService,
    ModalService,
    FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
