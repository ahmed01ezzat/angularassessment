import { Component, OnInit } from '@angular/core';
import {  AuthenticationService } from '../../services';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false
  constructor(
    private authenticationService: AuthenticationService,
    
  ) { 
  }

  ngOnInit() {
    if(localStorage.getItem('currentUser')){
     this.isLoggedIn = true;
   }else {
    this.isLoggedIn = false;
   }
  }
  logout(){
    return this.authenticationService.logout()
  }

}
