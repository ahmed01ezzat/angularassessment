import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ModalService } from '../../services/modal.service'
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../../services';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  private  users:  Array<object> = [];
  public  userToDelete =  {
    'username':'',
    'id':0
    }
    public UserDetails = {
      'avatar': "",
      'username': "",
      'id': 0
    }
    NewUser: FormGroup;
  
  public  count: Number = 1;
  constructor(
    private  RestService:  RestService,
    private modalService: ModalService, 
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getUsersData(this.count);
    this.NewUser = this.formBuilder.group({
      username: ['', Validators.required],
      job: ['', Validators.required]
  });
  }

  public  getUsersData(Page:Number){
    this.RestService.getUsers(Page).subscribe((data:  Array<object>) => {
        this.users  =  data;
    });
}
      openModal(id: string) {
          this.modalService.open(id);
      }
   
      closeModal(id: string) {
          this.modalService.close(id);
      }
    getUsername(user){
      this.userToDelete.username = user.first_name;
      this.userToDelete.id = user.id;
      this.openModal('custom-modal-1')

    }
    editUser(user){
      this.UserDetails = user;
      this.modalService.close('custom-modal-3')
      this.openModal('custom-modal-4')
    }
    deleteUser(){
      this.RestService.deleteUser(this.userToDelete.id).subscribe( 
        data => {
          this.alertService.success('Data Deleted successful', true);
           this.modalService.close('custom-modal-1');
           this.modalService.close('custom-modal-3');
                setTimeout(() => {
                  this.alertService.clear();
                },2000)
      },
      error => {
         this.modalService.close('custom-modal-1');
         this.modalService.close('custom-modal-3');
        console.log('err',error)
      })
    }
    SingleUser(user) {
      // console.log(user);
      this.UserDetails = user;
      console.log (this.UserDetails);
      this.openModal('custom-modal-3');
    }
    //easy access object params
    get f() { return this.NewUser.controls; }

    onSubmit() {
      // stop here if form is invalid
      if (this.NewUser.invalid) {
          return;
      }else {
        let obj = {
          'username':this.f.username.value, 
          'job':this.f.job.value
        }
        this.RestService.addUser(obj)
        this.alertService.success('Data Saved successful', true); 
              setTimeout(() => {
                this.alertService.clear();
              },2000)    
          
        
         this.modalService.close('custom-modal-2');
      }
    }
    onEdit(){
      if (this.NewUser.invalid) {
        return;
    }else {
      let obj = {
        'username':this.f.username.value, 
        'job':this.f.job.value
      }
      this.RestService.updateUser(obj)
      this.alertService.success('Data Saved successful', true); 
            setTimeout(() => {
              this.alertService.clear();
            },2000)    
        
      
       this.modalService.close('custom-modal-2');
      }
    }


}
