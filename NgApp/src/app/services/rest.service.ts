import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

// import {Observable} from 'rxjs/Rx';


@Injectable()
export class RestService {
    API_URL  =  'https://reqres.in/';
    constructor(private  httpClient:  HttpClient) {}
    getUsers(pageNum:Number){
        return  this.httpClient.get(`${this.API_URL}api/users?page=`+pageNum);
    }

    deleteUser (id: number): Observable<{}> {
        const url = `${this. API_URL}api/users/`+id; // DELETE api/user/id
        return this.httpClient.delete(url)
            .pipe(
            );
    }
    addUser(send){
        return  this.httpClient.post(this.API_URL+'api/users',send)
        .subscribe(
            data => {
                console.log("POST Request is successful ", data);
            },
            error => {
                console.log("Error", error);
            }
        ); 
      }
      updateUser(send){
        return  this.httpClient.post(this.API_URL+'api/users/2',send)
        .subscribe(
            data => {
                console.log("POST Request is successful ", data);
            },
            error => {
                console.log("Error", error);
            }
        ); 
      }
}